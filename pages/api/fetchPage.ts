import { sanityClient } from "@/sanity";
import type { NextApiRequest, NextApiResponse } from "next";


export default async function fetchPage(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    const pages = await sanityClient
      .fetch(`*[_type == "page"]`)
      .then((res) => res);

    return res.status(200).json({ pages });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Error: Can't fetch pages" });
  }
}
