import { sanityClient } from "@/sanity";
import type { NextApiRequest, NextApiResponse } from "next";


export default async function fetchBrand(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    const brands = await sanityClient
      .fetch(`*[_type == "brand"]`)
      .then((res) => res);

    return res.status(200).json({ brands });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Error: Can't fetch brands" });
  }
}
