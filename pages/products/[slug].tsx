import { sanityClient } from '@/sanity';
import ProductView from '@/src/views/ProductView'
import { GetStaticProps } from 'next';
import React from 'react'

interface Props{
  product: any
  variants: any
}

function Product({product, variants}:Props) {

  // console.log(product)

  return (
    <ProductView
    product={product}
    variants={variants}
    />
  )
}

export default Product

export async function getStaticPaths({locales}: any) {
    
  const query = `*[_type == "product" && slug!=null]{
      _id,
      slug {
          current
      }
  } `;

  const products = await sanityClient.fetch(query);
  
const paths = products
.map((product: any ) =>({
 
    params: { slug: product?.slug?.current },
    // locale, //locale should not be inside `params`
  })
)
.flat();

return {
    paths,
    fallback: false
}
}
 
export const getStaticProps: GetStaticProps = async ({params}:any) => {
    
  const productQuery= `*[_type == "product" && slug.current == $slug ][0] {
    _id,
    _createdAt,
    name,
    slug,
    mainImage,
    price,
    description,
    category->{
      name
    }
  
  }`

  const product = await sanityClient.fetch(productQuery, {
    slug: params?.slug,
})

if(!product){
    return {
        notFound: true
    }
}

//Fetch Variants of the Product query
const variantQuery = `
*[_type == "variant" && product._ref in*[_type=="product" && slug.current == $slug]._id]{
  _id,
  product->
  {
    _id,
    _createdAt,
    name,
    slug,
    mainImage,
    price,
    description,
    category->{
      name
    }
  },
  color->{
    value
  },
  mainImage,
  images
}`;

// Fetch data from external API
const variants = await sanityClient.fetch(variantQuery, {
  slug: params?.slug,
})

  
  return {
   props: {
    product,
    variants
  },
  revalidate: 10
 };
};