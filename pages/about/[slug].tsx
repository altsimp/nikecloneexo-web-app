import { sanityClient } from '@/sanity';
import PageView from '@/src/views/PageView'
import { GetStaticProps } from 'next';
import React from 'react'

interface Props{
  page: any
}

function Product({page}:Props) {

  // console.log(product)

  return (
    <PageView
    page={page}
    />
  )
}

export default Product

export async function getStaticPaths({locales}: any) {
    
  const query = `*[_type == "page" && slug!=null]{
      _id,
      slug {
          current
      }
  } `;

  const pages = await sanityClient.fetch(query);
  
const paths = pages
.map((page: any ) =>({
 
    params: { slug: page?.slug?.current },
    // locale, //locale should not be inside `params`
  })
)
.flat();

return {
    paths,
    fallback: false
}
}
 
export const getStaticProps: GetStaticProps = async ({params}:any) => {
    
  const pageQuery= `*[_type == "page" && slug.current == $slug ][0] {
    _id,
 title,
 subtitle,
  mainImage,
  description
  
  }`

  const page = await sanityClient.fetch(pageQuery, {
    slug: params?.slug,
})

if(!page){
    return {
        notFound: true
    }
}


  
  return {
   props: {
    page
  },
  revalidate: 10
 };
};