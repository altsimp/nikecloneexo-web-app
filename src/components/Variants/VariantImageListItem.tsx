import { urlFor } from '@/sanity';
import Image from 'next/image'
import React from 'react'

function VariantImageListItem({image}:any) {
  return (
    <li className="relative w-24 h-24 rounded-xl overflow-hidden">
      {image ? (
        <Image src={urlFor(image).url()!} fill className="object-cover" alt="" />
      ) : (
        <span>{`Pas d'image`}</span>
      )}
    </li>
  );
}

export default VariantImageListItem
