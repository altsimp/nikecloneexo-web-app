import Link from 'next/link'
import React from 'react'

function VariantInfo({name='Jordan 1', category='Chaussures', description='desc', price=199.99, color='', slug=''}:any) {
    return (
    <div
        className='flex flex-col py-3 space-y-1'
        >
            <Link
            href={`/products/${slug}`}
            className='font-medium hover:opacity-80'
            >
                {name}
            </Link>

            <p
            className='opacity-80 '
            >
                {category}
            </p>

            <p
            className='opacity-80 '
            >
                {color}
            </p>

            <p
            className='font-medium pt-1'
            >
                {price?.toFixed(2)} {'€'}
            </p>

            <p
            className='text-sm max-w-md'
            >
                {description}
            </p>
        </div>
  )
}

export default VariantInfo
