import { urlFor } from '@/sanity'
import React from 'react'
import VariantMainImage from './VariantMainImage'
import VariantImages from './VariantImages'
import VariantInfo from './VariantInfo'

interface Props{
  variant: any
 }

function VariantTopSection({variant}:Props) {
  return (
    <section
    className='flex w-full gap-10 p-14 justify-center'
    >
      <VariantImages
      images={variant?.images}
      />
      <VariantMainImage
      imageUrl={variant?.mainImage ? urlFor(variant?.mainImage).url()! : `/Images/hado.jpg`}
      />
      <VariantInfo
      name={variant?.product?.name}
      category={variant.product?.category?.name}
      description={variant.product?.description}
      price={variant?.product?.price}
      color={variant?.color?.value}
      slug={variant?.product?.slug?.current}
      />
    </section>
  )
}

export default VariantTopSection
