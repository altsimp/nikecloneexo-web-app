import Image from 'next/image'
import React from 'react'
import VariantImageListItem from './VariantImageListItem'

function VariantImages({images}:any) {
  return (
    <ul>
      {images && images.length > 0 
      ?
      images.map((image :any) =>
      <VariantImageListItem
      image={image}
      />
      )
      :
      <span>No images</span>
      }
    </ul>
  )
}

export default VariantImages
