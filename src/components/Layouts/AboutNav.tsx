import { urlFor } from '@/sanity';
import Link from 'next/link'
import React, { useEffect, useState } from 'react'
import useSWR from 'swr';
import SVG from 'react-inlinesvg'

function AboutNav() {
const [brands, setBrands] = useState<any>(null)
const [pages, setPages] = useState<any>(null)

const fetcher = (url:any) => fetch(url).then((res) => res.json());

const brandSwr = useSWR(`/api/fetchBrand`, fetcher,
{
  revalidateIfStale: true,
  revalidateOnFocus: true,
  revalidateOnReconnect: true
} )

const pageSwr = useSWR(`/api/fetchPage`, fetcher,
{
  revalidateIfStale: true,
  revalidateOnFocus: true,
  revalidateOnReconnect: true
} )

useEffect(() => {

    setBrands(brandSwr?.data?.brands)
  
  }, [brandSwr?.data]);

  useEffect(() => {

    setPages(pageSwr?.data?.pages)
  
  }, [pageSwr?.data]);
    
console.log(pages)

  return (
    <div
      className='hidden w-full px-12 py-2 bg-black bg-opacity-5 
      lg:flex justify-between items-center'
      >
        <ul
        className='flex flex-row space-x-3'
        >
            {brandSwr?.error && 
            <span>Error!</span>
            }

            {brandSwr?.isLoading && 
            <>
            <li
            className='w-7 h-7 bg-black rounded-full animate-pulse'
            >

            </li>

            <li
            className='w-7 h-7 bg-black rounded-full animate-pulse'
            >

            </li>
            </>
            }

            {brands && brands.length > 0 && 
            brands?.map((brand:any) => 
            <li
            className='w-7 h-7 '
            >
                {/* {brand?.name} */}
                <SVG
        src={
          brand?.mainImage
          ? urlFor(brand?.mainImage).url()! :
          "https://cdn.svgporn.com/logos/react.svg"}
        width={32}
        height={32}
        title={ brand?.name}
        fill='currentColor'
        preProcessor={(code:any) => code.replace(/fill=".*?"/g, 'fill="currentColor"')
      }
        className='text-black hover:text-red-500 transform ease-in-out duration-300'
      />
            </li>
            )
            }
        </ul>

        <nav
        className=''
        >
            <ul
            className='flex space-x-3 divide-x-2 divide-black items-center'
            >
                {pageSwr?.error && 
            <span>Error!</span>
            }

            {pageSwr?.isLoading && 
            <>
                <li
                className='pl-3 text-xs font-medium'
                >
                    <div
                   
                    className='hover:text-[#757575] hover:underline text-transparent bg-black animate-pulse'
                    >
                    <span>
                        {`Trouver un magasin`}
                    </span>
                    </div>
                </li>
                <li
                className='pl-3 text-xs font-medium'
                >
                <div
                   
                    className='hover:text-[#757575] hover:underline text-transparent bg-black animate-pulse'
                    >
                    <span>
                        {`Aide`}
                    </span>
                    </div>
                </li>
                </>
                }

                {pages && pages?.length > 0 &&
                pages.map((page:any) => 
                <li
                key={page?._id}
                className='pl-3 text-xs font-medium'
                >
                <Link
                    href={`/about/${page?.slug?.current}`}
                    className='hover:text-[#757575] hover:underline'
                    >
                    <span>
                        {page?.title}
                    </span>
                    </Link>
                </li>
                )
                }
                
                
            </ul>

        </nav>
      </div>
  )
}

export default AboutNav
