import React from 'react'
import ProductMainImage from './ProductMainImage'
import ProductInfo from './ProductInfo'
import { urlFor } from '@/sanity'

interface Props{
  product: any
}

function ProductTopSection({product}:Props) {
  return (
    <section
    className='flex flex-col lg:flex-row w-full gap-10 p-14 justify-center'
    >
      <ProductMainImage
      imageUrl={product?.mainImage ? urlFor(product?.mainImage).url()! : `/Images/hado.jpg`}
      />
      <ProductInfo
      name={product?.name}
      category={product?.category?.name}
      description={product?.description}
      price={product?.price}
      />
    </section>
  )
}

export default ProductTopSection
