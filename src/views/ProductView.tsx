import React from 'react'
import Layout from '../components/Layouts/Layout'
import ProductTopSection from '../components/Products/ProductTopSection'
import HomePageVariantSection from '../components/HomePage/HomePageVariantSection'

interface Props{
  product: any
  variants: any
}

function ProductView({product, variants}:Props) {

  return (
    <Layout>
      <ProductTopSection
      product={product}
      />

      <HomePageVariantSection
      variants={variants}
      />
    </Layout>
  )
}

export default ProductView
