import React from 'react'
import Layout from '../components/Layouts/Layout'
import Image from 'next/image'
import { urlFor } from '@/sanity'

function PageView({page}:any) {
  return (
    
    <Layout>
        <section
         className='w-full h-[550px] relative overflow-hidden flex'
    >
      <Image
      src={page?.mainImage ? urlFor(page?.mainImage).url()! : `/Images/nike-just-do-it.jpg`}
      alt={``}
      fill
      className='object-contain opacity-50'
      />
        <div
    className='absolute flex flex-col space-y-4 w-full items-center flex-1 text-black text-center'
    >
        <h1
        className='max-w-lg mx-auto font-bold uppercase text-2xl'>
            {page?.title}
        </h1>
        <h2
        className='max-w-lg mx-auto text-lg font-medium'>
            {page?.subtitle}
        </h2>

        <p
        className='max-w-lg mx-auto'>
            {page?.description}
        </p>
      
    </div>
    </section>
    </Layout>
  )
}

export default PageView
